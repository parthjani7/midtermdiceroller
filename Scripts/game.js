// Name : Parth Jani
// Student ID : 300984336
// Date : June 25, 2018

let app;
(function(app) {
  "use strict";

  // Game Variables
  let stage;
  let canvas;
  let dice1;
  let dice2;
  let dicelable1;
  let dicelable2;
  let assetManager;
  let startButton;

  let manifest = [
      { id: "one", src: "/Assets/images/1.png" },
      { id: "two", src: "/Assets/images/2.png" },
      { id: "three", src: "/Assets/images/3.png" },
      { id: "four", src: "/Assets/images/4.png" },
      { id: "five", src: "/Assets/images/5.png" },
      { id: "six", src: "/Assets/images/6.png" },
      { id: "blank", src: "/Assets/images/blank.png" },
      { id: "StartButton", src: "/Assets/images/StartButton.png" }
    ];

  function Init() {
      console.log("App Initializing...");
      assetManager = new createjs.LoadQueue();
      assetManager.installPlugin(createjs.Sound);
      assetManager.on("complete", Start);
      assetManager.loadManifest(manifest);
  }


  /**
   * The Start function initializes the createjs Stage object,
   * sets the framerate and sets up the Main Game Loop to
   * trigger every frame
   *
   */
  function Start() {
    console.log("App Started...");
    canvas = document.getElementById("canvas");
    stage = new createjs.Stage(canvas);
    stage.enableMouseOver(20);
    createjs.Ticker.framerate = 60;
    createjs.Ticker.on("tick", Update);

    Main();
  }

  /**
   * This is the Main Game Loop it runs at 60 fps
   *
   */
  function Update() {
    stage.update();
  }

  /**
   *  This is the main function - place all your code here
   *
   */
  function Main() {
    console.log("Main Function...");

    // hello label
    dice1 = new createjs.Bitmap(assetManager.getResult("blank"));
    dice1.regX = dice1.getBounds().width * 0.5;
    dice1.regY = dice1.getBounds().height * 0.5;
    dice1.x = 150;
    dice1.y = 150;
    stage.addChild(dice1);

    dice2 = new createjs.Bitmap(assetManager.getResult("blank"));
    dice2.regX = dice2.getBounds().width * 0.5;
    dice2.regY = dice2.getBounds().height * 0.5;
    dice2.x = 450;
    dice2.y = 150;
    stage.addChild(dice2);

    dicelable1 = new createjs.Text(" ", "20px Consolas", "#000000");
    dicelable1.regX = dicelable1.getBounds().width * 0.5;
    dicelable1.regY = dicelable1.getBounds().height * 0.5;
    dicelable1.x = 150;
    dicelable1.y = 300;
    stage.addChild(dicelable1);

    dicelable2 = new createjs.Text(" ", "20px Consolas", "#000000");
    dicelable2.regX = dicelable2.getBounds().width * 0.5;
    dicelable2.regY = dicelable2.getBounds().height * 0.5;
    dicelable2.x = 450;
    dicelable2.y = 300;
    stage.addChild(dicelable2);

    // start button
    startButton = new createjs.Bitmap(assetManager.getResult("StartButton"));
    startButton.regX = startButton.getBounds().width * 0.5;
    startButton.regY = startButton.getBounds().height * 0.5;
    startButton.x = 320;
    startButton.y = 400;
    stage.addChild(startButton);

    // start button listeners
    startButton.addEventListener("click", function() {
        var random1=Math.floor((Math.random() * 6) + 1);
        var random2=Math.floor((Math.random() * 6) + 1);

        var img1=getDiceImage(random1);
        var img2=getDiceImage(random2);

        dice1.image=assetManager.getResult(img1);
        dicelable1.text=img1;

        dice2.image=assetManager.getResult(img2);
        dicelable2.text=img2;

    });

    startButton.addEventListener("mouseover", function(event) {
        event.currentTarget.alpha = 0.7;
    });

    startButton.addEventListener("mouseout", function(event) {
        event.currentTarget.alpha = 1.0;
    });
  }

  function getDiceImage(random){
    switch (random) {
      case 1:
        return "one";
      case 2:
        return "two";
      case 3:
        return "three";
      case 4:
        return "four";
      case 5:
        return "five";
      case 6:
        return "six";
    }
    return "blank";
  }

  window.addEventListener("load", Init);
})(app | (app = {}));
